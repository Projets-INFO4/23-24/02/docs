**Fiche de Route : Projet Tableaux de Bord Intelligents**

---

*15 janvier :*

- Sélection du sujet : Tableaux (Intelligents) de Bord pour le Suivi des Capteurs de Température dans les Bâtiments du CROUS Grenoble.

*29 janvier :*

- Présentation et explication du sujet par M. Didier DONSEZ.
- Début de la lecture de la documentation fournie par le professeur sur Nodered - Influxdb - Grafana.
- Installation de Docker.

*12 février :*

- Initiation à Graphana et NodeRed.
- Correction de quelques problèmes du code fourni avec M. Didier DONSEZ. 
- recuperation de la liste des capteurs à utiliser.

*19 février :*

- Réunion avec nos encadrants Gita MOOTOOSAMY (CROUS Grenoble) et Germain LEMASSON.
- Explication des besoins par Madame Gita.

*4 mars :*

- Début de la modification de l'affichage selon les exigences de Madame Gita.

*11 mars :*

- Présentation des avancements à nos encadrants Gita MOOTOOSAMY et Germain LEMASSON.

*12 mars :*

- Mise en place d'une carte affichant les emplacements des capteurs.
- Essayer d'utiliser les gateways pour détecter les éventuels déplacements des capteurs, car les capteurs ne sont pas équipés de GPS. 

*15 mars :*

- Soutenance de mi-parcours.

*24 mars :*

- Utilisation des variables pour utiliser une liste déroulante dans le dashboard.
- Mise en place de code couleur pour la batterie
- Tests avec OpenWeatherMap

*25 mars :*
- Réunion avec Gita MOOTOOSAMY et Germain LEMASSON.
- Présentation des avancements à nos encadrants Gita MOOTOOSAMY et Germain LEMASSON.

*29 mars :*
- gestion des utilisateurs et des acces, aprés réception des identifients Grafana.
- ajout l'option de voir l'historique des données.
- amélioration du code couleur pour la batterie selon les besoins de nos encadrents.

*02 avril :*
- présentation d'une version Beta à notre encadrante Gita et à l'un des utilisateurs finaux de notre produit (un agent du CROUS).
- l'utilisateur final nous indique l'importance de faire des alerts en cas de baisse ou augmentation critique dans les températures.
- notes des dérnières retouches pour finaliser notre projet.


--- 
