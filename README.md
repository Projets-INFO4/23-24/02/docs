## Description

Docs for [**Polytech Grenoble INFO4 8th semester project**](https://air.imag.fr/index.php/Projets_2023-2024#INFO4) n°2 : *Dashboard_CROUS_info4_2023_2024*

## Table of contents

- [**Code repository**](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/02/dashboard-crous)

- [**Follow-up sheet**](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/02/docs/-/blob/main/Dashboard_CROUS_info4_2023_2024.md?ref_type=heads)

- [**Mid-term Presentation slide**](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/02/dashboard-crous/-/blob/main/diapo_mi-soutenance.pptx.pdf?ref_type=heads)

- [**Final report pdf**](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/23-24/02/docs/-/blob/main/rapport.pdf?ref_type=heads)

## Authors

- Abderahmane Mesbah @mesbahab
- Abdelhakim Benhamed @benhamea
- Brice Decurninge @decurnib
- Achraf Khribech @khribeca

